IDENTIFICATION DIVISION.
        PROGRAM-ID.theobol.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.

        SELECT ftheatres ASSIGN TO "theatres.dat"
        ORGANIZATION sequential
        ACCESS IS sequential
        FILE STATUS IS fthe_stat.

        SELECT fpieces ASSIGN TO "pieces.dat"
        ORGANIZATION indexed
        ACCESS IS dynamic
        RECORD KEY fpie_num
        ALTERNATE RECORD KEY fpie_type  with DUPLICATES
        FILE STATUS IS fpie_stat.

        SELECT frepresentations  ASSIGN TO "representations.dat"
        ORGANIZATION indexed
        ACCESS IS dynamic
        RECORD KEY frep_num
        ALTERNATE RECORD KEY frep_numPie  with DUPLICATES
        ALTERNATE RECORD KEY frep_numThe   with DUPLICATES
        FILE STATUS IS frep_stat.

        SELECT fclients ASSIGN TO "clients.dat"
        ORGANIZATION indexed
        ACCESS IS dynamic
        RECORD KEY fcli_num
        ALTERNATE RECORD KEY fcli_cle  with DUPLICATES
        ALTERNATE RECORD KEY fcli_numThe with DUPLICATES
        FILE STATUS IS fcli_stat.

        SELECT fhistoriques ASSIGN TO "historiques.dat"
        ORGANIZATION indexed
        ACCESS IS dynamic
        RECORD KEY fhis_num
        ALTERNATE RECORD KEY fhis_numClient  with DUPLICATES
        ALTERNATE RECORD KEY fhis_numRep  with DUPLICATES
        FILE STATUS IS fhis_stat.

DATA DIVISION.
FILE SECTION.
FD ftheatres.
        01 theTamp.
                02 fthe_num PIC 9(4).
                02 fthe_nbSalles PIC 9.
                02 fthe_nbClients PIC 9(4).
                02 fthe_nom PIC A(30).
                02 fthe_ville PIC A(30).
                02 fthe_adresse PIC A(30).

FD fpieces.
        01 pieTamp.
                02 fpie_num PIC 9(4).
                02 fpie_nom PIC A(30).
                02 fpie_metteurEnScene PIC A(30).
                02 fpie_acteurs PIC A(150).
                02 fpie_type  PIC 9.
                02 fpie_duree PIC 9(3).

FD frepresentations.
        01 repTamp.
                02 frep_num PIC 9(4).
                02 frep_numPie PIC 9(4).
                02 frep_numThe PIC 9(4).
                02 frep_date PIC A(10).
                02 frep_horaire PIC A(10).
                02 frep_prix PIC 9(2).

FD fclients.
        01 cliTamp.
                02 fcli_num PIC 9(4).
                02 fcli_cle.
                        03 fcli_nom PIC A(30).
                        03 fcli_prenom PIC A(30).
                02 fcli_numThe PIC 9(4).

FD fhistoriques.
        01 hisTamp.
                02 fhis_num  PIC 9(4).
                02 fhis_numRep PIC 9(4).
                02 fhis_numClient  PIC 9(4).

WORKING-STORAGE SECTION.
        77 fthe_stat PIC 9(2).
        77 fpie_stat PIC 9(2).
        77 frep_stat PIC 9(2).
        77 fcli_stat PIC 9(2).
        77 fhis_stat PIC 9(2).
        77 Wverif PIC 9.
        77 Wrep PIC 9.
        77 Wter PIC 9.
        77 Wid PIC 9(4).
        77 Wnom PIC A(30).
        77 Wville PIC A(30).
        77 WnumThe PIC 9(4).
        77 WnumRep PIC 9(4).
        77 WnumSal PIC 9(4).
        77 Wtype PIC 9.
        77 Wprenom PIC A(30).
        77 WnumPie PIC 9(4).
        77 Whoraire PIC A(10).
        77 Wdate PIC A(10).
        77 WnomPie PIC A(30).
        77 WsommeDuree PIC 9(10).
        77 Wcpt PIC 9(10).
        77 Wresult PIC 9(10).
        77 WnomCli PIC A(30).
        77 Wsortie1 PIC A(30).
        77 Wsortie2 PIC A(30).
        77 Wsortie3 PIC A(30).
        77 Wsortie4 PIC A(30).
        77 Wfonc PIC 9(2).

PROCEDURE DIVISION.
        OPEN INPUT ftheatres
        IF fthe_stat = 35 THEN
            OPEN OUTPUT ftheatres
        END-IF
        CLOSE ftheatres

        OPEN INPUT fpieces
        IF fpie_stat = 35 THEN
            OPEN OUTPUT fpieces
        END-IF
        CLOSE fpieces

        OPEN INPUT frepresentations
        IF frep_stat = 35 THEN
            OPEN OUTPUT frepresentations
        END-IF
        CLOSE frepresentations

        OPEN INPUT fclients
        IF fcli_stat = 35 THEN
            OPEN OUTPUT fclients
        END-IF
        CLOSE fclients

        OPEN INPUT fhistoriques
        IF fhis_stat = 35 THEN
            OPEN OUTPUT fhistoriques
        END-IF
        CLOSE fhistoriques

PERFORM WITH TEST AFTER UNTIL Wfonc = 0
    DISPLAY '---------------------------------'
    DISPLAY 'Quelle action voulez vous faire, choisissez un numero :'
    DISPLAY '(1) AJOUT_THEATRE'
    DISPLAY '(2) AFFICHER_THEATRES'
    DISPLAY '(3) AJOUT_PIECE'
    DISPLAY '(4) AFFICHER_PIECES'
    DISPLAY '(5) AJOUT_REPRESENTATION'
    DISPLAY '(6) AFFICHER_REPRESENTATIONS'
    DISPLAY '(7) AJOUT_CLIENT'
    DISPLAY '(8) AFFICHER_CLIENTS'
    DISPLAY '(9) AJOUT_HISTORIQUE'
    DISPLAY '(10) AFFICHER_HISTORIQUES'
    DISPLAY '(11) RECHERCHE_REP_PIECE_THEATRE'
    DISPLAY '(12) RECHERCHE_REP_THEATRE'
    DISPLAY '(13) AFFICHE_CLIENT_THEATRE'
    DISPLAY '(14) RECHERCHE_TYPE_REP_THEATRE'
    DISPLAY '(15) RECHERCHE_REP_DATE_VILLE'
    DISPLAY '(16) DUREE_MOY_REP_CLIENT'
    DISPLAY '(17) DUREE_MOY_REP_THEATRE'
    DISPLAY '(18) DUREE_MOY_REP'
    DISPLAY '(19) NB_MOYEN_SPECTATEURS_PIECE'
    DISPLAY '(20) SUPPRIMER_CLIENT'
    DISPLAY '(21) SUPPRIMER_REPRESENTATION'
    DISPLAY '(22) MODIF_HORAIRE_DATE_REP'
    DISPLAY '(0) Sortir'
    ACCEPT Wfonc
    EVALUATE Wfonc
        WHEN 1
            PERFORM AJOUT_THEATRE
        WHEN 2
            PERFORM AFFICHER_THEATRES
        WHEN 3
            PERFORM AJOUT_PIECE
        WHEN 4
            PERFORM AFFICHER_PIECES
        WHEN 5
            PERFORM AJOUT_REPRESENTATION
        WHEN 6
            PERFORM AFFICHER_REPRESENTATIONS
        WHEN 7
            PERFORM AJOUT_CLIENT
        WHEN 8
            PERFORM AFFICHER_CLIENTS
        WHEN 9
            PERFORM AJOUT_HISTORIQUE
        WHEN 10
            PERFORM AFFICHER_HISTORIQUES
        WHEN 11
            PERFORM RECHERCHE_REP_PIECE_THEATRE
        WHEN 12
            PERFORM RECHERCHE_REP_THEATRE
        WHEN 13
            PERFORM AFFICHE_CLIENT_THEATRE
        WHEN 14
            PERFORM RECHERCHE_TYPE_REP_THEATRE
        WHEN 15
            PERFORM RECHERCHE_REP_DATE_VILLE
        WHEN 16
            PERFORM DUREE_MOY_REP_CLIENT
        WHEN 17
            PERFORM DUREE_MOY_REP_THEATRE
        WHEN 18
            PERFORM DUREE_MOY_REP
        WHEN 19
            PERFORM NB_MOYEN_SPECTATEURS_PIECE
        WHEN 20
            PERFORM SUPPRIMER_CLIENT
        WHEN 21
            PERFORM SUPPRIMER_REPRESENTATION
        WHEN 22
            PERFORM MODIF_HORAIRE_DATE_REP
        WHEN OTHER
            MOVE 0 TO Wfonc
        END-PERFORM

    STOP RUN.


        AJOUT_THEATRE.
            MOVE 0 TO Wid
            MOVE 0 TO Wrep
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                MOVE 0 TO Wverif
                OPEN INPUT ftheatres
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ ftheatres
                    AT END
                        MOVE 1 TO Wverif
                        MOVE Wnom TO fthe_nom
                        MOVE Wville TO fthe_ville
                        MOVE 1 TO Wrep
                    NOT AT END
                        MOVE fthe_num TO Wid
                        IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                            DISPLAY "Nom deja present dans cette ville"
                            DISPLAY "Veuillez choisir un nouveau nom :"
                            ACCEPT Wnom
                            MOVE 1 TO Wverif
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE ftheatres
            END-PERFORM
            IF Wid = 0 THEN
                MOVE 1 TO fthe_num
            ELSE
                MOVE Wid TO fthe_num
                ADD 1 TO fthe_num
            END-IF
            MOVE 0 TO fthe_nbSalles
            MOVE 0 TO fthe_nbClients
            DISPLAY 'adresse du theatre ?'
            ACCEPT fthe_adresse
            OPEN EXTEND ftheatres
            WRITE theTamp
            END-WRITE
            CLOSE ftheatres.

        AFFICHER_THEATRES.
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    DISPLAY theTamp
                END-READ
            END-PERFORM
            CLOSE ftheatres.

        AJOUT_PIECE.
            MOVE 0 TO Wid
            MOVE 0 TO Wrep
            DISPLAY 'nom de la piece ?'
            ACCEPT Wnom
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                MOVE 0 TO Wverif
                OPEN INPUT fpieces
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ fpieces NEXT
                    AT END
                        MOVE 1 TO Wverif
                        MOVE Wnom TO fpie_nom
                        MOVE 1 TO Wrep
                    NOT AT END
                        MOVE fpie_num TO Wid
                        IF Wnom = fpie_nom THEN
                            DISPLAY "Nom deja utilise"
                            DISPLAY "Veuillez choisir un nouveau nom :"
                            ACCEPT Wnom
                            MOVE 1 TO Wverif
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE fpieces
            END-PERFORM
            IF Wid = 0 THEN
                MOVE 1 TO fpie_num
            ELSE
                MOVE Wid TO fpie_num
                ADD 1 TO fpie_num
            END-IF
            DISPLAY 'nom du metteur en scene ?'
            ACCEPT fpie_metteurEnScene
            DISPLAY 'acteurs jouant dans cette piece ?'
            ACCEPT fpie_acteurs
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY "choisissez le chiffre correspondant au type :"
                DISPLAY "1 : Comedie"
                DISPLAY "2 : Tragi-comedie"
                DISPLAY "3 : Tragedie"
                ACCEPT fpie_type
                IF fpie_type < 4 AND fpie_type > 0 THEN
                    MOVE 1 TO Wrep
                END-IF
            END-PERFORM
            DISPLAY 'duree de la piece en minute ?'
            ACCEPT fpie_duree
            OPEN I-O fpieces
            WRITE pieTamp
            INVALID KEY
                DISPLAY "is pas ok"
            NOT INVALID KEY
                DISPLAY "is ok"
            END-WRITE
            CLOSE fpieces.

        AFFICHER_PIECES.
            OPEN INPUT fpieces
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fpieces NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    DISPLAY pieTamp
                END-READ
            END-PERFORM
            CLOSE fpieces.

        AJOUT_REPRESENTATION.
            MOVE 0 TO Wid
            MOVE 0 TO Wverif
            OPEN INPUT frepresentations
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ frepresentations NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    MOVE frep_num TO Wid
                END-READ
            END-PERFORM
            CLOSE frepresentations
            IF Wid = 0 THEN
                MOVE 1 TO frep_num
            ELSE
                MOVE Wid TO frep_num
                ADD 1 TO frep_num
            END-IF
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY 'nom de la piece ?'
                ACCEPT Wnom
                OPEN INPUT fpieces
                MOVE 0 TO Wverif
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ fpieces NEXT
                    AT END
                        MOVE 1 TO Wverif
                        DISPLAY 'Aucune piece trouve'
                        DISPLAY 'Veuillez entrer de nouvelles infos'
                    NOT AT END
                        IF Wnom = fpie_nom THEN
                            MOVE fpie_num TO frep_numPie
                            MOVE 1 TO Wverif
                            MOVE 1 TO Wrep
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE fpieces
            END-PERFORM
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY 'nom du theatre ?'
                ACCEPT Wnom
                DISPLAY 'ville du theatre ?'
                ACCEPT Wville
                OPEN INPUT ftheatres
                MOVE 0 TO Wverif
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ ftheatres
                    AT END
                        MOVE 1 TO Wverif
                        DISPLAY 'Aucun theatre trouve'
                        DISPLAY 'Veuillez entrer de nouvelles infos'
                    NOT AT END
                        IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                            MOVE fthe_num TO frep_numThe
                            MOVE 1 TO Wverif
                            MOVE 1 TO Wrep
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE ftheatres
            END-PERFORM
            DISPLAY "Quelle date ?"
            ACCEPT frep_date
            DISPLAY "Quel horaire ?"
            ACCEPT frep_horaire
            DISPLAY "Quel prix ?"
            ACCEPT frep_prix
            OPEN I-O frepresentations
            WRITE repTamp
            INVALID KEY
                DISPLAY "is pas ok"
            NOT INVALID KEY
                DISPLAY "is ok"
            END-WRITE
            CLOSE frepresentations.

        AFFICHER_REPRESENTATIONS.
            OPEN INPUT frepresentations
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ frepresentations NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    DISPLAY repTamp
                END-READ
            END-PERFORM
            CLOSE frepresentations.

        AJOUT_CLIENT.
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            MOVE 0 TO Wter
            PERFORM WITH TEST AFTER UNTIL Wter = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wter
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        MOVE 0 TO Wid
                        MOVE 0 TO Wrep
                        DISPLAY 'nom du client ?'
                        ACCEPT Wnom
                        DISPLAY 'prenom du client ?'
                        ACCEPT Wprenom
                        PERFORM WITH TEST AFTER UNTIL Wrep = 1
                            MOVE 0 TO Wverif
                            OPEN INPUT fclients
                            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                                READ fclients NEXT
                                AT END
                                    MOVE 1 TO Wverif
                                    MOVE Wnom TO fcli_nom
                                    MOVE Wprenom TO fcli_prenom
                                    MOVE WnumThe TO fcli_numThe
                                    MOVE 1 TO Wrep
                                NOT AT END
                                    MOVE fcli_num TO Wid
                                    IF Wnom = fcli_nom
                                    AND Wprenom = fcli_prenom
                                    AND WnumThe = fcli_numThe
                                    THEN
                                        DISPLAY "Homonyme"
                                        DISPLAY "Veuillez reessayer"
                                        DISPLAY "Nouveau nom :"
                                        ACCEPT Wnom
                                        DISPLAY "Nouveau prenom :"
                                        ACCEPT Wprenom
                                        MOVE 1 TO Wverif
                                    END-IF
                                END-READ
                            END-PERFORM
                            CLOSE fclients
                        END-PERFORM
                        IF Wid = 0 THEN
                            MOVE 1 TO fcli_num
                        ELSE
                            MOVE Wid TO fcli_num
                            ADD 1 TO fcli_num
                        END-IF
                        MOVE 1 TO Wter
                        OPEN I-O fclients
                        WRITE cliTamp
                        INVALID KEY
                            DISPLAY "is pas ok"
                        NOT INVALID KEY
                            DISPLAY "is ok"
                        END-WRITE
                        CLOSE fclients
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres.

        AFFICHER_CLIENTS.
            OPEN INPUT fclients
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fclients NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    DISPLAY cliTamp
                END-READ
            END-PERFORM
            CLOSE fclients.

        AJOUT_HISTORIQUE.
            MOVE 0 TO Wid
            MOVE 0 TO Wverif
            OPEN INPUT fhistoriques
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fhistoriques NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    MOVE fhis_num TO Wid
                END-READ
            END-PERFORM
            CLOSE fhistoriques
            IF Wid = 0 THEN
                MOVE 1 TO fhis_num
            ELSE
                MOVE Wid TO fhis_num
                ADD 1 TO fhis_num
            END-IF
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY 'nom de la piece ?'
                ACCEPT Wnom
                OPEN INPUT fpieces
                MOVE 0 TO Wverif
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ fpieces NEXT
                    AT END
                        MOVE 1 TO Wverif
                        DISPLAY 'Aucune piece trouve'
                        DISPLAY 'Veuillez entrer de nouvelles infos'
                    NOT AT END
                        IF Wnom = fpie_nom THEN
                            MOVE fpie_num TO WnumPie
                            MOVE 1 TO Wverif
                            MOVE 1 TO Wrep
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE fpieces
            END-PERFORM
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY 'nom du theatre ?'
                ACCEPT Wnom
                DISPLAY 'ville du theatre ?'
                ACCEPT Wville
                OPEN INPUT ftheatres
                MOVE 0 TO Wverif
                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                    READ ftheatres
                    AT END
                        MOVE 1 TO Wverif
                        DISPLAY 'Aucun theatre trouve'
                        DISPLAY 'Veuillez entrer de nouvelles infos'
                    NOT AT END
                        IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                            MOVE fthe_num TO WnumThe
                            MOVE 1 TO Wverif
                            MOVE 1 TO Wrep
                        END-IF
                    END-READ
                END-PERFORM
                CLOSE ftheatres
            END-PERFORM
            DISPLAY 'horaire de la representation ?'
            ACCEPT Whoraire
            DISPLAY 'Date de la representation ?'
            ACCEPT Wdate
            MOVE WnumPie TO frep_numPie
            MOVE 0 TO Wverif
            OPEN INPUT frepresentations
            START frepresentations, KEY = frep_numPie
                INVALID KEY
                    DISPLAY "aucune repésentation pour cette piece"
                NOT INVALID KEY
                    PERFORM WITH TEST AFTER UNTIL Wverif = 1
                        READ frepresentations NEXT
                        AT END
                            MOVE 1 TO Wverif
                        NOT AT END
                            IF frep_horaire = Whoraire AND
                            frep_date = Wdate
                            AND frep_numThe = WnumThe THEN
                                MOVE frep_num TO fhis_numRep
                                MOVE 1 TO Wverif
                            END-IF
                        END-READ
                    END-PERFORM
            END-START
            CLOSE frepresentations
            IF Wverif = 1 THEN
                MOVE 0 TO Wrep
                PERFORM WITH TEST AFTER UNTIL Wrep = 1
                    DISPLAY 'nom du client ?'
                    ACCEPT Wnom
                    DISPLAY 'prenom du client ?'
                    ACCEPT Wprenom
                    OPEN INPUT fclients
                    MOVE 0 TO Wverif
                    PERFORM WITH TEST AFTER UNTIL Wverif = 1
                        READ fclients NEXT
                        AT END
                            MOVE 1 TO Wverif
                            DISPLAY 'Aucun client trouve'
                            DISPLAY 'Veuillez entrer de nouvelles infos'
                        NOT AT END
                            IF Wnom = fcli_nom AND Wprenom = fcli_prenom
                            AND WnumThe = fcli_numThe THEN
                                MOVE fcli_num TO fhis_numClient
                                MOVE 1 TO Wverif
                                MOVE 1 TO Wrep
                            END-IF
                        END-READ
                    END-PERFORM
                    CLOSE fclients
                END-PERFORM
                OPEN I-O fhistoriques
                WRITE hisTamp
                INVALID KEY
                    DISPLAY "is pas ok"
                NOT INVALID KEY
                    DISPLAY "is ok"
                END-WRITE
                CLOSE fhistoriques
            END-IF.

        AFFICHER_HISTORIQUES.
            OPEN INPUT fhistoriques
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fhistoriques NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    DISPLAY hisTamp
                END-READ
            END-PERFORM
            CLOSE fhistoriques.

        RECHERCHE_REP_PIECE_THEATRE.
            DISPLAY "nom de la piece"
            ACCEPT WnomPie
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT fpieces
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fpieces NEXT
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucune piece trouve'
                NOT AT END
                    IF WnomPie = fpie_nom THEN
                        MOVE fpie_num TO WnumPie
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE fpieces
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres
            MOVE WnumPie TO frep_numPie
            MOVE 0 TO Wverif
            OPEN INPUT frepresentations
            START frepresentations, KEY = frep_numPie
                INVALID KEY
                    DISPLAY "aucune repésentation pour cette piece"
                NOT INVALID KEY
                    PERFORM WITH TEST AFTER UNTIL Wverif = 1
                        READ frepresentations NEXT
                        AT END
                            MOVE 1 TO Wverif
                            DISPLAY "Pas de representation"
                        NOT AT END
                            IF frep_numThe = WnumThe
                            AND frep_numPie = Wnumpie THEN
                                STRING "Nom  de la piece :" WnomPie
                                INTO Wsortie1
                                STRING "Date :" frep_date
                                INTO Wsortie2
                                STRING "Horaire :" frep_horaire
                                INTO Wsortie3
                                STRING "Prix :" frep_prix
                                INTO Wsortie4
                                DISPLAY Wsortie1
                                DISPLAY Wsortie2
                                DISPLAY Wsortie3
                                DISPLAY Wsortie4
                                DISPLAY "-----------------"
                            END-IF
                            IF frep_numPie <> Wnumpie THEN
                                MOVE 1 TO Wverif
                            END-IF
                        END-READ
                    END-PERFORM
                END-START
                CLOSE frepresentations.

        RECHERCHE_REP_THEATRE.
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        OPEN INPUT frepresentations
                        OPEN INPUT fpieces
                        MOVE 0 TO Wrep
                        PERFORM WITH TEST AFTER UNTIL Wrep = 1
                            READ frepresentations NEXT
                            AT END
                                MOVE 1 TO Wrep
                                IF Wter = 0 THEN
                                    DISPLAY "Aucune représentation"
                                END-IF
                            NOT AT END
                                IF frep_numThe = WnumThe THEN
                                    MOVE frep_numPie TO fpie_num
                                    READ fpieces
                                    INVALID KEY
                                        DISPLAY "pas ok piece"
                                    NOT INVALID KEY
                                        MOVE fpie_nom TO WnomPie
                                    END-READ
                                    STRING "Nom  de la piece :" WnomPie
                                    INTO Wsortie1
                                    STRING "Date :" frep_date
                                    INTO Wsortie2
                                    STRING "Horaire :" frep_horaire
                                    INTO Wsortie3
                                    STRING "Prix :" frep_prix
                                    INTO Wsortie4
                                    DISPLAY Wsortie1
                                    DISPLAY Wsortie2
                                    DISPLAY Wsortie3
                                    DISPLAY Wsortie4
                                    DISPLAY "-----------------"
                                END-IF
                                MOVE 1 TO Wter
                            END-READ
                        END-PERFORM
                        CLOSE fpieces
                        CLOSE frepresentations
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres.

        AFFICHE_CLIENT_THEATRE.
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        MOVE WnumThe TO fcli_numThe
                        OPEN INPUT fclients
                        MOVE 0 TO Wverif
                        START fclients, KEY = fcli_numThe
                            INVALID KEY
                                DISPLAY "aucun client pour ce theatre"
                            NOT INVALID KEY
                                PERFORM WITH TEST AFTER UNTIL Wverif = 1
                                    READ fclients NEXT
                                    AT END
                                        MOVE 1 TO Wverif
                                    NOT AT END
                                        IF WnumThe = fcli_numThe THEN
                                            STRING "Id :" fcli_num
                                            INTO Wsortie2
                                            STRING "Nom :" fcli_nom
                                            INTO Wsortie3
                                            STRING "Prenom :"fcli_prenom
                                            INTO Wsortie4
                                            DISPLAY Wsortie2
                                            DISPLAY Wsortie3
                                            DISPLAY Wsortie4
                                            DISPLAY "-----------------"
                                        ELSE
                                            MOVE 1 TO Wverif
                                        END-IF
                                    END-READ
                                END-PERFORM
                        END-START
                        CLOSE fclients
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres.

        RECHERCHE_TYPE_REP_THEATRE.
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                DISPLAY "choisissez le chiffre correspondant au type :"
                DISPLAY "1 : Comedie"
                DISPLAY "2 : Tragi-comedie"
                DISPLAY "3 : Tragedie"
                ACCEPT Wtype
                IF Wtype < 4 AND Wtype > 0 THEN
                    MOVE 1 TO Wrep
                END-IF
            END-PERFORM
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        MOVE 1 TO Wverif
                        MOVE 0 TO Wter
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres
            IF Wter = 0 THEN
                MOVE Wtype TO  fpie_type
                OPEN INPUT fpieces
                MOVE 0 TO Wrep
                START fpieces, KEY = fpie_type
                    INVALID KEY
                        DISPLAY "aucune piece pour ce type"
                    NOT INVALID KEY
                        PERFORM WITH TEST AFTER UNTIL Wrep = 1
                            READ fpieces NEXT
                            AT END
                                MOVE 1 TO Wrep
                            NOT AT END
                                MOVE fpie_num TO frep_numPie
                                MOVE fpie_nom TO WnomPie
                                MOVE 0 TO Wverif
                                OPEN INPUT frepresentations
                                START frepresentations, 
                                KEY = frep_numPie
                                    INVALID KEY
                                        MOVE 1 TO Wverif
                                    NOT INVALID KEY
                                        PERFORM WITH TEST
                                        AFTER UNTIL Wverif = 1
                                            READ frepresentations NEXT
                                            AT END
                                                MOVE 1 TO Wverif
                                                DISPLAY "Aucune rep"
                                            NOT AT END
                                            IF frep_numThe = WnumThe
                                            AND frep_numPie = fpie_num
                                            AND fpie_type = Wtype THEN
                                                STRING "Piece :"
                                                WnomPie
                                                INTO Wsortie1
                                                STRING "Date :"
                                                frep_date
                                                INTO Wsortie2
                                                STRING "Horaire :"
                                                frep_horaire
                                                INTO Wsortie3
                                                STRING "Prix :"
                                                frep_prix
                                                INTO Wsortie4
                                                DISPLAY Wsortie1
                                                DISPLAY Wsortie2
                                                DISPLAY Wsortie3
                                                DISPLAY Wsortie4
                                                DISPLAY "---------"
                                            END-IF
                                            IF frep_numPie <> fpie_num
                                            THEN
                                                MOVE 1 TO Wverif
                                            END-IF
                                            END-READ
                                        END-PERFORM
                                    END-START
                                    CLOSE frepresentations
                                    IF fpie_type <> Wtype THEN
                                        MOVE 1 TO Wrep
                                    END-IF
                            END-READ
                        END-PERFORM
                END-START
                CLOSE fpieces
            END-IF.

        RECHERCHE_REP_DATE_VILLE.
            DISPLAY "Date ?"
            ACCEPT Wdate
            DISPLAY "Ville ?"
            ACCEPT Wville
            MOVE 0 TO Wverif
            OPEN INPUT frepresentations
            OPEN INPUT fpieces
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ frepresentations NEXT
                AT END
                    MOVE 1 TO Wverif
                NOT AT END
                    IF Wdate = frep_date THEN
                        MOVE frep_numPie TO fpie_num
                        READ fpieces
                        INVALID KEY
                            DISPLAY "pas ok piece"
                        NOT INVALID KEY
                            MOVE fpie_nom TO WnomPie
                        END-READ
                        OPEN INPUT ftheatres
                        MOVE 0 TO Wrep
                        PERFORM WITH TEST AFTER UNTIL Wrep = 1
                        READ ftheatres
                        AT END
                            MOVE 1 TO Wrep
                        NOT AT END
                            IF Wville = fthe_ville
                            AND fthe_num = frep_numThe THEN
                                STRING "Piece :" WnomPie
                                INTO Wsortie1
                                STRING "Date :" frep_date
                                INTO Wsortie2
                                STRING "Horaire :" frep_horaire
                                INTO Wsortie3
                                STRING "Prix :" frep_prix
                                INTO Wsortie4
                                DISPLAY Wsortie1
                                DISPLAY Wsortie2
                                DISPLAY Wsortie3
                                DISPLAY Wsortie4
                                DISPLAY "---------"
                            END-IF
                        END-READ
                        END-PERFORM
                        CLOSE ftheatres
                    END-IF
                END-READ
            END-PERFORM
            CLOSE fpieces
            CLOSE frepresentations.

        DUREE_MOY_REP_CLIENT.
            DISPLAY "nom du client"
            ACCEPT Wnom
            DISPLAY "prenom du client"
            ACCEPT Wprenom
            MOVE Wnom TO fcli_nom
            MOVE Wprenom TO fcli_prenom
            OPEN INPUT fclients
            OPEN INPUT fhistoriques
            OPEN INPUT frepresentations
            OPEN INPUT fpieces
            MOVE 0 TO Wverif
            MOVE 0 TO WsommeDuree
            MOVE 0 TO Wcpt
            START fclients, KEY = fcli_cle
                INVALID KEY
                    DISPLAY "aucun client pour ces informations"
                NOT INVALID KEY
                    PERFORM WITH TEST AFTER UNTIL Wverif = 1
                        READ fclients NEXT
                        AT END
                            MOVE 1 TO Wverif
                        NOT AT END
                        IF fcli_nom = Wnom AND fcli_prenom = Wprenom
                            MOVE fcli_num TO fhis_numClient
                            MOVE 0 TO Wrep
                            START fhistoriques, KEY = fhis_numClient
                                INVALID KEY
                                    DISPLAY "aucune rep pour ce client"
                                NOT INVALID KEY
                                    PERFORM WITH TEST
                                    AFTER UNTIL Wrep = 1
                                        READ fhistoriques NEXT
                                        AT END
                                            MOVE 1 TO Wrep
                                        NOT AT END
                                            MOVE fhis_numRep TO frep_num
                                            READ frepresentations
                                            INVALID KEY
                                                DISPLAY "pas ok rep"
                                            NOT INVALID KEY
                                                MOVE frep_numPie
                                                TO fpie_num
                                                READ fpieces
                                                INVALID KEY
                                                    DISPLAY "pas ok pie"
                                                NOT INVALID KEY
                                                    ADD 1 TO Wcpt
                                                    ADD fpie_duree
                                                    TO WsommeDuree
                                                END-READ
                                            END-READ
                                        END-READ
                                    END-PERFORM
                            END-START
                        END-IF
                        END-READ
                    END-PERFORM
            END-START
            CLOSE fpieces
            CLOSE frepresentations
            CLOSE fhistoriques
            CLOSE fclients
            DIVIDE WsommeDuree BY Wcpt GIVING Wresult
            DISPLAY "La duree moyenne d'une representation pour :"
            DISPLAY Wnom
            DISPLAY Wprenom
            DISPLAY "est :"
            DISPLAY Wresult.

        DUREE_MOY_REP_THEATRE.
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE fthe_num TO WnumThe
                        OPEN INPUT fhistoriques
                        OPEN INPUT frepresentations
                        OPEN INPUT fpieces
                        MOVE 0 TO Wverif
                        MOVE 0 TO WsommeDuree
                        MOVE 0 TO Wcpt
                        PERFORM WITH TEST AFTER UNTIL Wverif = 1
                            READ fhistoriques NEXT
                            AT END
                                MOVE 1 TO Wverif
                                DISPLAY 'Aucun historique trouve'
                            NOT AT END
                                MOVE fhis_numRep TO frep_num
                                DISPLAY fhis_numRep
                                READ frepresentations
                                INVALID KEY
                                    DISPLAY "pas ok rep"
                                NOT INVALID KEY
                                    IF frep_numThe = WnumThe THEN
                                        MOVE frep_numPie TO fpie_num
                                        READ fpieces
                                        INVALID KEY
                                            DISPLAY "pas ok piece"
                                        NOT INVALID KEY
                                            ADD 1 TO Wcpt
                                            ADD fpie_duree
                                            TO WsommeDuree
                                        END-READ
                                    END-IF
                                END-READ
                                MOVE 1 TO Wverif
                            END-READ
                        END-PERFORM
                        CLOSE fpieces
                        CLOSE frepresentations
                        CLOSE fhistoriques
                        DIVIDE WsommeDuree BY Wcpt GIVING Wresult
                        DISPLAY "La duree moyenne d'une rep pour :"
                        DISPLAY Wnom
                        DISPLAY "à"
                        DISPLAY Wville
                        DISPLAY "est :"
                        DISPLAY Wresult
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE ftheatres.

        DUREE_MOY_REP.
            OPEN INPUT fhistoriques
            OPEN INPUT frepresentations
            OPEN INPUT fpieces
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fhistoriques NEXT
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun historique trouve'
                NOT AT END
                    MOVE fhis_numRep TO frep_num
                    READ frepresentations
                    INVALID KEY
                        DISPLAY "pas ok rep"
                    NOT INVALID KEY
                        MOVE frep_numPie TO fpie_num
                        READ fpieces
                        INVALID KEY
                            DISPLAY "pas ok piece"
                        NOT INVALID KEY
                            ADD 1 TO Wcpt
                            ADD fpie_duree TO WsommeDuree
                        END-READ
                    END-READ
                    MOVE 1 TO Wverif
                END-READ
            END-PERFORM
            CLOSE fpieces
            CLOSE frepresentations
            CLOSE fhistoriques
            DIVIDE WsommeDuree BY Wcpt GIVING Wresult
            DISPLAY "La duree moyenne d'une representation est :"
            DISPLAY Wresult.

        NB_MOYEN_SPECTATEURS_PIECE.
            DISPLAY "nom de la piece"
            ACCEPT WnomPie
            OPEN INPUT fpieces
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ fpieces NEXT
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucune piece trouve'
                NOT AT END
                    IF WnomPie = fpie_nom THEN
                        MOVE fpie_num TO WnumPie
                        MOVE 1 TO Wverif
                    END-IF
                END-READ
            END-PERFORM
            CLOSE fpieces
            MOVE WnumPie TO frep_numPie
            MOVE 0 TO Wverif
            MOVE 0 TO WsommeDuree
            MOVE 0 TO Wcpt
            OPEN INPUT frepresentations
            OPEN INPUT fhistoriques
            START frepresentations, KEY = frep_numPie
                INVALID KEY
                    DISPLAY "aucune repésentation pour cette piece"
                NOT INVALID KEY
                    PERFORM WITH TEST AFTER UNTIL Wverif = 1
                        READ frepresentations NEXT
                        AT END
                            MOVE 1 TO Wverif
                        NOT AT END
                            MOVE frep_num TO fhis_numRep
                            MOVE 0 TO Wrep
                            IF WnumPie = frep_numPie THEN
                            ADD 1 TO Wcpt
                            START fhistoriques, KEY = fhis_numRep
                                INVALID KEY
                                    MOVE 1 TO Wverif
                                NOT INVALID KEY
                                    PERFORM WITH TEST
                                    AFTER UNTIL Wrep = 1
                                        READ fhistoriques NEXT
                                        AT END
                                            MOVE 1 TO Wrep
                                        NOT AT END
                                            IF fhis_numRep = frep_num
                                            THEN
                                                DISPLAY 'A'
                                                ADD 1 TO WsommeDuree
                                            END-IF
                                        END-READ
                                    END-PERFORM
                            END-START
                            END-IF
                        END-READ
                    END-PERFORM
            END-START
            CLOSE fhistoriques
            CLOSE frepresentations
            DISPLAY WsommeDuree
            DISPLAY Wcpt
            DIVIDE WsommeDuree BY Wcpt GIVING Wresult
            DISPLAY "Le nombre de spectateur moyen pour :"
            DISPLAY WnomPie
            DISPLAY "est :"
            DISPLAY Wresult.

        SUPPRIMER_CLIENT.
            DISPLAY "nom du client ?"
            ACCEPT WnomCli
            DISPLAY "prenom du client ?"
            ACCEPT Wprenom
            DISPLAY 'nom du theatre ?'
            ACCEPT Wnom
            DISPLAY 'ville du theatre ?'
            ACCEPT Wville
            OPEN INPUT ftheatres
            OPEN I-O fclients
            MOVE 0 TO Wverif
            PERFORM WITH TEST AFTER UNTIL Wverif = 1
                READ ftheatres
                AT END
                    MOVE 1 TO Wverif
                    DISPLAY 'Aucun theatre trouve'
                NOT AT END
                    IF Wnom = fthe_nom AND Wville = fthe_ville THEN
                        MOVE 0 TO Wrep
                        PERFORM WITH TEST AFTER UNTIL Wrep = 1
                            READ fclients NEXT
                            AT END
                                MOVE 1 TO Wrep
                                DISPLAY 'Aucun theatre trouve'
                            NOT AT END
                                IF WnomCli = fcli_nom
                                AND Wprenom = fcli_prenom
                                AND fcli_numThe = fthe_num THEN
                                    MOVE fcli_num To WnumThe
                                    MOVE 1 TO Wrep
                                    MOVE 1 TO Wverif
                                END-IF
                            END-READ
                        END-PERFORM
                    END-IF
                END-READ
            END-PERFORM
            MOVE WnumThe TO fcli_num
            READ fclients
            INVALID KEY
                DISPLAY "problème de suppression"
            NOT INVALID KEY
                DELETE fclients RECORD
                DISPLAY "suppression effectuee"
            END-READ
            CLOSE fclients
            CLOSE ftheatres.

        SUPPRIMER_REPRESENTATION.
            DISPLAY "nom de la piece"
            ACCEPT WnomPie
            OPEN INPUT fpieces
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                READ fpieces NEXT
                AT END
                    MOVE 1 TO Wrep
                    DISPLAY 'Aucune piece trouve'
                NOT AT END
                    IF WnomPie = fpie_nom THEN
                        DISPLAY 'nom du theatre ?'
                        ACCEPT Wnom
                        DISPLAY 'ville du theatre ?'
                        ACCEPT Wville
                        OPEN INPUT ftheatres
                        MOVE 0 TO Wverif
                        PERFORM WITH TEST AFTER UNTIL Wverif = 1
                            READ ftheatres
                            AT END
                                MOVE 1 TO Wverif
                                DISPLAY 'Aucun theatre trouve'
                            NOT AT END
                                IF Wnom = fthe_nom
                                AND Wville = fthe_ville THEN
                                    DISPLAY "date de la rep ?"
                                    ACCEPT Wdate
                                    DISPLAY "horaire de la rep ?"
                                    ACCEPT Whoraire
                                    OPEN INPUT frepresentations
                                    MOVE 0 TO Wter
                                    MOVE fpie_num TO frep_numPie
                                    START frepresentations,
                                    KEY = frep_numPie
                                        INVALID KEY
                                            DISPLAY "aucune rep"
                                        NOT INVALID KEY
                                            PERFORM WITH TEST
                                            AFTER UNTIL Wter = 1
                                                READ frepresentations
                                                NEXT
                                                AT END
                                                    MOVE 1 TO Wter
                                                    DISPLAY "Aucune rep"
                                                NOT AT END
                                                    IF frep_horaire
                                                    = Whoraire AND
                                                    frep_date
                                                    = Wdate
                                                    AND frep_numThe =
                                                    fthe_num THEN
                                                        MOVE frep_num
                                                        TO WnumRep
                                                        MOVE 1 TO Wter
                                                        MOVE 1 TO Wcpt
                                                    END-IF
                                                END-READ
                                            END-PERFORM
                                        END-START
                                        CLOSE frepresentations
                                    MOVE 1 TO Wverif
                                END-IF
                            END-READ
                        END-PERFORM
                        CLOSE ftheatres
                        MOVE 1 TO Wrep
                    END-IF
                END-READ
            END-PERFORM
            CLOSE fpieces
            IF Wcpt = 1 THEN
                MOVE WnumRep TO frep_num
                OPEN I-O frepresentations
                READ frepresentations
                INVALID KEY
                    DISPLAY "pas ok rep"
                NOT INVALID KEY
                    DELETE frepresentations RECORD
                    DISPLAY "suppression effectuee"
                END-READ
                CLOSE frepresentations
            END-IF.

        MODIF_HORAIRE_DATE_REP.
            DISPLAY "nom de la piece"
            ACCEPT WnomPie
            OPEN INPUT fpieces
            MOVE 0 TO Wrep
            PERFORM WITH TEST AFTER UNTIL Wrep = 1
                READ fpieces NEXT
                AT END
                    MOVE 1 TO Wrep
                    DISPLAY 'Aucune piece trouve'
                NOT AT END
                    IF WnomPie = fpie_nom THEN
                        DISPLAY 'nom du theatre ?'
                        ACCEPT Wnom
                        DISPLAY 'ville du theatre ?'
                        ACCEPT Wville
                        OPEN INPUT ftheatres
                        MOVE 0 TO Wverif
                        PERFORM WITH TEST AFTER UNTIL Wverif = 1
                            READ ftheatres
                            AT END
                                MOVE 1 TO Wverif
                                DISPLAY 'Aucun theatre trouve'
                            NOT AT END
                                IF Wnom = fthe_nom
                                AND Wville = fthe_ville THEN
                                    DISPLAY "date de la rep ?"
                                    ACCEPT Wdate
                                    DISPLAY "horaire de la rep ?"
                                    ACCEPT Whoraire
                                    OPEN INPUT frepresentations
                                    MOVE 0 TO Wter
                                    MOVE fpie_num TO frep_numPie
                                    START frepresentations,
                                    KEY = frep_numPie
                                        INVALID KEY
                                            DISPLAY "aucune rep"
                                        NOT INVALID KEY
                                            PERFORM WITH TEST
                                            AFTER UNTIL Wter = 1
                                                READ frepresentations
                                                NEXT
                                                AT END
                                                    MOVE 1 TO Wter
                                                    DISPLAY "Aucune rep"
                                                NOT AT END
                                                    IF frep_horaire
                                                    = Whoraire AND
                                                    frep_date
                                                    = Wdate
                                                    AND frep_numThe =
                                                    fthe_num THEN
                                                        MOVE frep_num
                                                        TO WnumRep
                                                        MOVE 1 TO Wter
                                                        MOVE 1 TO Wcpt
                                                    END-IF
                                                END-READ
                                            END-PERFORM
                                        END-START
                                        CLOSE frepresentations
                                    MOVE 1 TO Wverif
                                END-IF
                            END-READ
                        END-PERFORM
                        CLOSE ftheatres
                        MOVE 1 TO Wrep
                    END-IF
                END-READ
            END-PERFORM
            CLOSE fpieces
            IF Wcpt = 1 THEN
                MOVE WnumRep TO frep_num
                OPEN I-O frepresentations
                READ frepresentations
                INVALID KEY
                    DISPLAY "pas ok rep"
                NOT INVALID KEY
                    DISPLAY "Nouvel horaire de la representation ?"
                    ACCEPT frep_horaire
                    DISPLAY "Nouvelle date de la representation ?"
                    ACCEPT frep_date
                    REWRITE repTamp
                    DISPLAY "Modificatoins effectuees"
                END-READ
                CLOSE frepresentations
            END-IF.
